from django.http import HttpResponse
from django.shortcuts import render, redirect

def index(request):
    response = {}
    return render(request,"index.html", response)

def story1(request):
    response = {}
    return render(request, "story1.html", response)

def story3(request):
    response = {}
    return render(request, "story3.html", response)

def quotes(request):
    response = {}
    return render(request, "quotes.html", response)

def designs(request):
    response = {}
    return render(request, "designs.html", response)

